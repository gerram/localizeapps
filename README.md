# README #

### What is this repository for? ###

* Translate app's strings


### Current apps ###

#### Fifteens (The Boss Puzzle) ####
* itunes.apple.com
[us](https://itunes.apple.com/us/app/the-boss-puzzle/id627858212),
[de](https://itunes.apple.com/de/app/the-boss-puzzle/id627858212),
[fr](https://itunes.apple.com/fr/app/the-boss-puzzle/id627858212),
[es](https://itunes.apple.com/es/app/the-boss-puzzle/id627858212),
[ja](https://itunes.apple.com/jp/app/the-boss-puzzle/id627858212),
[zh](https://itunes.apple.com/cn/app/the-boss-puzzle/id627858212),
[ru](https://itunes.apple.com/ru/app/the-boss-puzzle/id627858212)

* [Support page for Fifteens (The Boss Puzzle)](http://memoryk.com/en/pages/games/boss/)
* DescriptionApp_xx.txt (File with app description and keywords)
* Changes_xx_i.i.i.txt (File with changes of new release)
* Localizable_xx.strings (File with strings)


```
#!


```
