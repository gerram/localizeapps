/* Description */
"With game 'The Boss puzzle' you will spend your time for the good and won't allow your brain to be a lazy. Move the tiles until you get a set of numbers from 1 to 15 and from left to right. Everything is important - and time, and moves! Obtain your statuses until Senior, Pro, Expert, Faster, Sage. Challenge your friends." = "玩拼图益智游戏，可以给您带来益处，而不只是放松大脑。移动拼图，从左到右数字从1到15整体排序，最重要的是-消耗的时间和移动的数量。获得的称号：领主，职业，专家，敏捷，哲人。快邀请您们的朋友打破记录吧！";

/* Keywords */
"15,slide,column,row,board,family,square,casual,children,cool,loyd,algorithm,move,logic,block" = "拼图，15，十五，免费，合理性，玩，发展，记忆力，想，决定，休息，数字，智慧。";
