/* Description */
"With game 'The Boss puzzle' you will spend your time for the good and won't allow your brain to be a lazy. Move the tiles until you get a set of numbers from 1 to 15 and from left to right. Everything is important - and time, and moves! Obtain your statuses until Senior, Pro, Expert, Faster, Sage. Challenge your friends." = "Boss puzzle使用すると、有益な時間を過ごせるとともに、脳を活性化することができます。
1から15までの番号を左から右に並べ終わるまでタイルを移動します。
時間と移動数などのすべてが重要です。
上級者、プロ、エキスパート、Faster、哲人までのステータスを取得できます。
ぜひ友人に挑戦してみてください。";

/* Keywords */
"15,slide,column,row,board,family,square,casual,children,cool,loyd,algorithm,move,logic,block" = "15,15 - パズル、スライド、正方形、カジュアル、子供、クール、ロイド、アルゴリズム、ツイスター、移動、自由な、論理、組み合わせ";
