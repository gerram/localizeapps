/* Description */
"With game 'The Boss puzzle' you will spend your time for the good and won't allow your brain to be a lazy. Move the tiles until you get a set of numbers from 1 to 15 and from left to right. Everything is important - and time, and moves! Obtain your statuses until Senior, Pro, Expert, Faster, Sage. Challenge your friends." = "Mit dem Spiel 'Das Boss Puzzle' werden Sie ihre Zeit für einen guten Zweck verwenden und geben ihrem Gehirn keine Chance sich auszuruhen. Bewegen Sie die Kacheln von links nach rechts bis sie einen Satz von den Zahlen 1 bi 15 erhalten. Alles ist wichtig, Zeit und Schritte! Erlangen Sie verschiedne Status wie Senior, Pro, Experte, Schneller, Weise. Fordern Sie Ihre Freunde heraus.";

/* Keywords */
"15,slide,column,row,board,family,square,casual,children,cool,loyd,algorithm,move,logic,block" = "15,15-puzzle,kachel,quadrat,locker,kinder,cool,Algorithmus,denksport,zug,free,kostenlos,logik";
